# README

_**Warning! This was developed to aid my (Peter J. Christopher) PhD. Use this at your own risk!**_

## Introduction

We have a Thermoteknix Miricle 307K in our labs (CMMPE, University of Cambridge). I've found it very useful, here's a CAD model. I made this myself from measurements so it is liable to be innacurate. 

![Thermoteknix Miricle 307K](miricle-large.png)

## Notes

* Also available [here](https://grabcad.com/library/thermoteknix-miricle-307k-1)

## License

Copyright 2019 Peter J. Christopher

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Written by Peter J. Christopher - peterjchristopher@gmail.com, pjc209@cam.ac.uk